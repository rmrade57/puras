"""ABC Problem modul"""

import string

class ABCProblem:
    """ABCProblem class"""
    def __init__(self):
        self.blocks = [("B", "O"), ("X", "K"), ("D", "Q"), ("C", "P"), ("N", "A"),
                       ("G", "T"), ("R", "E"), ("T", "G"), ("Q", "D"), ("F", "S"),
                       ("J", "W"), ("H", "U"), ("V", "I"), ("A", "N"), ("O", "B"),
                       ("E", "R"), ("F", "S"), ("L", "Y"), ("P", "C"), ("Z", "M")]

    def can_make_word(self, word):

        word = word.upper()

        for letter in word:
            if not letter.isalpha():
                return False

            found_letter = False
            for i, block in enumerate(self.blocks):
                if letter in block:
                    found_letter = True
                    self.blocks.pop(i)
                    break
            if not found_letter:
                return False

        return True

    def DisplayWordAndResult(self, word):
        result = self.can_make_word(word)
        output = word + " | " + str(result)
        print(output)
        return output
