"""Test modul for ABCProblem class"""

import unittest

from ABCProblem import ABCProblem


class TestABCProblem(unittest.TestCase):
    """Test class for ABCProblem class"""

    def setUp(self):
        self.ABCProblem = ABCProblem()

    def test_should_confirm_that_single_letter_word_can_be_spelled(self):
        """Testing Scenario_1 - Single letter word as input"""
        self.assertTrue(self.ABCProblem.can_make_word("A"))

    def test_should_confirm_that_multi_letter_word_can_be_spelled(self):
        """Testing Scenario_2 - Multi letter words as input"""
        self.assertTrue(self.ABCProblem.can_make_word("PURAS"))

    def test_should_confirm_that_multi_letter_word_can_not_be_spelled(self):
        """Testing Scenario_2 - Multi letter words as input"""
        self.assertFalse(self.ABCProblem.can_make_word("COMMON"))

    def test_should_confirm_that_word_can_not_contain_special_characters(self):
        """Testing Scenario_3 - Special character in word as input"""
        self.assertFalse(self.ABCProblem.can_make_word("!nput."))

    def test_should_confirm_that_word_can_not_contain_numbers(self):
        """Testing Scenario_3 - Special character in word as input"""
        self.assertFalse(self.ABCProblem.can_make_word("w8"))

    def test_should_confirm_that_function_is_case_insensitive(self):
        """Testing Scenario_4 - Case sensitivity"""
        self.assertTrue(self.ABCProblem.can_make_word("CoNfUsE"))

class DisplayWordAndResult(unittest.TestCase):
    """Test class for DisplayWordAndBlocks method"""

    def setUp(self):
        self.ABCProblem = ABCProblem ()

    def test_should_confirm_that_function_displays_word_and_result(self):
        """Testing Scenario_5 - Display word and function output in "word | result" format"""
        self.assertEqual(self.ABCProblem.DisplayWordAndResult("sQuAd"), "sQuAd | True")

if __name__ == '__main__':
    unittest.main()
