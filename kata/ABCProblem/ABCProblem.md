# Feature: ABC Problem

## Narrative:

**As a** customer
**I want** to check if a given word can be spelled using a collection of ABC blocks.
**So that** I can play with the ABC blocks and spell words.

## Scenario_1: Single letter words

**Given**: Any single letter word <br>
**When**: Any single letter word entered <br>
**Then**: Return if a word could be spelled with given ABC blocks <br>

## Scenario_2: Multi letter words

**Given**: Any multi letter word <br>
**When**: Any multi letter word entered <br>
**Then**: Return if a word could be spelled with given ABC blocks <br>

## Scenario_3: Special characters in a word

**Given**: Any word that contains special character <br>
**When**: Any word with special characters in construction entered <br>
**Then**: Word should not be spelled <br>

## Scenario_4: Case sensitivity

**Given**: Any word containing upper and lower case letters <br>
**When**: Any word containing upper and lower case letters entered <br>
**Then**: Return the same result as if the word was in lower case <br>

## Scenario_5: Display word and function output in "word | result" format

**Given**: Any word <br>
**When**: Any word entered <br>
**Then**: Display word and result <br>
